package com.classpath.orders.model;

import static lombok.AccessLevel.PRIVATE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor(access = PRIVATE )
@Builder
@AllArgsConstructor

@Entity
@Table
@ToString(exclude = "order")
@EqualsAndHashCode(exclude = "order")
@Setter
@Getter
public class LineItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int qty;
	private String name;
	private double pricePerUnit;
	
	@ManyToOne
	@JoinColumn(name="order_id_fk", nullable = false)
	@JsonBackReference
	private Order order;

}
