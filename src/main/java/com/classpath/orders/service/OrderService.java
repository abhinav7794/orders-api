package com.classpath.orders.service;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.classpath.orders.dto.OrderDto;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderRepository orderRepository;

	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}

	public Map<String, Object> fetchAllOrders(int page, int size, String strDirection, String property) {

		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

		PageRequest pageRequest = PageRequest.of(page, size, direction, property);

		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

		long totalNumberOfElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		List<Order> content = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total-records", totalNumberOfElements);
		responseMap.put("total-pages", totalPages);
		responseMap.put("data", content);

		return responseMap;
	}

	public Order fetchOrderById(long id) {
		return this.orderRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

	public void deleteOrderById(long id) {
		this.orderRepository.deleteById(id);
	}

	public Map<String, Object> fetchOrdersByPriceRange(double min, double max, int page, int size, String strDirection,
			String property) {

		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

		PageRequest pageRequest = PageRequest.of(page, size, direction, property);

		Page<OrderDto> pageResponse = this.orderRepository.findByPriceBetween(min, max, pageRequest);

		long totalNumberOfElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		List<OrderDto> content = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total-records", totalNumberOfElements);
		responseMap.put("total-pages", totalPages);
		responseMap.put("data", content);

		return responseMap;
	}
}