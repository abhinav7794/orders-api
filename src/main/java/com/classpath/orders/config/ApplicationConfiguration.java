package com.classpath.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {
	
	@Bean
	@ConditionalOnProperty(prefix="app", name="loadUser", havingValue="true", matchIfMissing = true)
	public User user() {
		return new User();
	}
	
	@Bean
	@ConditionalOnBean(name="user")
	public User userBasedOnBean() {
		return new User();
	}
	
	@Bean
	@ConditionalOnMissingBean(name="user")
	public User userBasedOnMissingBean() {
		return new User();
	}
	
	@Bean
	@ConditionalOnMissingClass(value ="com.classpath.orders.DemoApplication")
	public User userBasedOnMissingClass() {
		return new User();
	}
}

class User {

}
