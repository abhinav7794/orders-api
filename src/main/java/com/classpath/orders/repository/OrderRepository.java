package com.classpath.orders.repository;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpath.orders.dto.OrderDto;
import com.classpath.orders.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
    Page<OrderDto> findByPriceBetween(double min, double max, Pageable pageable);
    Page<Order> findByOrderDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);
}
